# Why?

This project tries to **investigate and provide tests for the nodejs package setups and configurations**
to understand how to build a packages.json, the files inside, etc. using npm.
The changes over the last years, like 
- `type: module`
- `module: filename.js` 
- ECMAScript modules (esm)
- and other options

that one can use with a nodejs package to build and use a package has made it quite
hard to understand what works and how. At least that was the case for me while building
[pico-tester](https://codeberg.org/wolframkriesing/pico-tester).

## How come?
So I started this project which uses the excellent
[smoke](https://github.com/SamirTalwar/smoke/) "an integration test framework for practically anything"
to test and describe the different setups and ensure the learnings I make
are validated and don't just evaporate after trying it out.

Eventually this might be useful for testing against different nodejs versions.
And maybe there is also a suite of tests for nodejs for that already, I didn't search for it.
If so, this is double work and was just a learning for me, then I would suggest to go look
at the other tests, not those here.

## How to use?
You need to have docker.
- `./run.sh bash` starts a docker container where everything is installed inside
  which is nodejs and smoke, you will land in a bash shell
  - now install the node deps, like so `cd src; npm install; cd -`
  - `smoke test` runs all the tests
- after the `npm i` (see above ran once) from outside the docker container 
  `./run.sh smoke test` will start the tests and report all results to the console

## Progress is in the CHANGELOG.md
The [CHANGELOG.md](./CHANGELOG.md) file is used to describe and list the things that had been done
and the roadmap of what is planned. I am using it in a mikado method way, so that I list
the achievements and tick them off or list any deeper level tasks below them.
If this just causes more question marks, see the file and it might make sense.