# Roadmap
- use require inside a type=module package, which I've seen fail
- use `import`
- use `require`
- mix `import` and `require`

# v1
- [x] setup smoke to run tests
- [x] use mjs files
- [x] build easy to understand structure to test different package.jsons, module files, and combination of any
  might evolve while working on it